package com.ilhanli.omer.dagger2sample.di.module;

import android.app.Activity;
import android.content.Context;

import com.ilhanli.omer.dagger2sample.business.AccountApiImpl;
import com.ilhanli.omer.dagger2sample.business.service.AccountApi;
import com.ilhanli.omer.dagger2sample.di.scope.ActivityContext;
import com.ilhanli.omer.dagger2sample.di.scope.ApplicationContext;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {

        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return activity.getBaseContext();
    }

    @Provides
    AccountApi provideAccountApi() {

        return new AccountApiImpl(activity);
    }
}
