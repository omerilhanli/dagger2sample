package com.ilhanli.omer.dagger2sample.utility;

import android.app.Activity;
import android.content.Intent;

import com.ilhanli.omer.dagger2sample.common.BundleAttribute;
import com.ilhanli.omer.dagger2sample.data.model.Employee;

public class TransationUtil {

    public static void startActivity(Activity activity, Class<?> targetClass){

        Intent intent = new Intent(activity, targetClass);

        activity.startActivity(intent);

        activity.finish();
    }

    public static void startActivity(Activity activity, Class<?> targetClass, Employee employee){

        Intent intent = new Intent(activity, targetClass);

        intent.putExtra(BundleAttribute.EMPLOYEE, employee);

        activity.startActivity(intent);

        activity.finish();
    }

}
