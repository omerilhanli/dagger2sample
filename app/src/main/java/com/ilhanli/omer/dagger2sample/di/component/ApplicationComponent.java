package com.ilhanli.omer.dagger2sample.di.component;

import android.app.Application;
import android.content.Context;

import com.ilhanli.omer.dagger2sample.App;
import com.ilhanli.omer.dagger2sample.business.AccountApiImpl;
import com.ilhanli.omer.dagger2sample.business.service.AccountApi;
import com.ilhanli.omer.dagger2sample.data.DataManager;
import com.ilhanli.omer.dagger2sample.data.DbHelper;
import com.ilhanli.omer.dagger2sample.data.SharedPrefsHelper;
import com.ilhanli.omer.dagger2sample.di.module.ApplicationModule;
import com.ilhanli.omer.dagger2sample.di.scope.ApplicationContext;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Provides;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(App application);

    @ApplicationContext
    Context getcontext();

    Application getApplication();

    DataManager getDataManager();

    SharedPrefsHelper getPreferenceHelper();

    DbHelper getDbHelper();

}
