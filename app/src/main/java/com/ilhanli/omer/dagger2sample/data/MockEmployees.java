package com.ilhanli.omer.dagger2sample.data;

import com.ilhanli.omer.dagger2sample.data.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class MockEmployees {

    private static List<Employee> employees;

    static {

        employees = new ArrayList<>();

        for (int i = 0; i < 3; i++) {

            Employee employee = new Employee();

            employee.setEmployeeId(i);

            employee.setEmployeeSalary((i + 1) * 3000);

            employee.setEmployeePassword("000" + i);

            switch (i) {

                case 0:
                    employee.setEmployeeFirstName("Ömer");

                    employee.setEmployeeLastName("İlhanlı");

                    employee.setEmployeeUsername("omer");

                    break;

                case 1:

                    employee.setEmployeeFirstName("Employee0" + i);

                    employee.setEmployeeFirstName("EmployeeLast0" + i);

                    employee.setEmployeeUsername("employee" + i);

                    break;

                case 2:

                    employee.setEmployeeFirstName("Employee0" + i);

                    employee.setEmployeeFirstName("EmployeeLast0" + i);

                    employee.setEmployeeUsername("employee" + i);

                    break;

                default:
                    break;
            }

            employees.add(employee);
        }
    }

    public static List<Employee> employees() {

        return employees;
    }

}
