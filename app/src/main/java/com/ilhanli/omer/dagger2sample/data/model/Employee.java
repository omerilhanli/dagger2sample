package com.ilhanli.omer.dagger2sample.data.model;

import java.io.Serializable;

public class Employee implements Serializable {

    private static final long serialVersionUID = 7830280741917037377L;

    private int employeeId;
    private String employeeFirstName;
    private String employeeLastName;
    private String employeeUsername;
    private String employeePassword;
    private int employeeSalary;

    private boolean loginStatus;

    public Employee() {
    }

    public Employee(int employeeId, String employeeFirstName,
                    String employeeLastName, String employeeUsername,
                    String employeePassword, int employeeSalary, boolean loginStatus) {
        this.employeeId = employeeId;
        this.employeeFirstName = employeeFirstName;
        this.employeeLastName = employeeLastName;
        this.employeeUsername = employeeUsername;
        this.employeePassword = employeePassword;
        this.employeeSalary = employeeSalary;
        this.loginStatus = loginStatus;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public void setEmployeeFirstName(String employeeFirstName) {
        this.employeeFirstName = employeeFirstName;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public void setEmployeeLastName(String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }

    public String getEmployeeUsername() {
        return employeeUsername;
    }

    public void setEmployeeUsername(String employeeUsername) {
        this.employeeUsername = employeeUsername;
    }

    public String getEmployeePassword() {
        return employeePassword;
    }

    public void setEmployeePassword(String employeePassword) {
        this.employeePassword = employeePassword;
    }

    public int getEmployeeSalary() {
        return employeeSalary;
    }

    public void setEmployeeSalary(int employeeSalary) {
        this.employeeSalary = employeeSalary;
    }

    public boolean isLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(boolean loginStatus) {
        this.loginStatus = loginStatus;
    }
}
