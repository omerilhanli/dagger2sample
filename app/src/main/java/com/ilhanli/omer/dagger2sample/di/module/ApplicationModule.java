package com.ilhanli.omer.dagger2sample.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.ilhanli.omer.dagger2sample.business.AccountApiImpl;
import com.ilhanli.omer.dagger2sample.business.service.AccountApi;
import com.ilhanli.omer.dagger2sample.di.scope.ActivityContext;
import com.ilhanli.omer.dagger2sample.di.scope.ApplicationContext;
import com.ilhanli.omer.dagger2sample.di.scope.DatabaseInfo;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Application mApplication;

    public ApplicationModule(Application application) {

        this.mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {

        return mApplication;
    }

    @Provides
    Application provideApplication() {

        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {

        return "demo-dagger.db";
    }

    @Provides
    @DatabaseInfo
    Integer provideDatabaseVersion() {

        return 2;
    }

    @Provides
    SharedPreferences provideSharedPreference() {

        return mApplication.getSharedPreferences("demo-prefs", Context.MODE_PRIVATE);
    }
}
