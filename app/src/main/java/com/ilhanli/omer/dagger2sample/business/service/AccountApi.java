package com.ilhanli.omer.dagger2sample.business.service;

import com.ilhanli.omer.dagger2sample.business.Operation;
import com.ilhanli.omer.dagger2sample.data.model.Employee;

public interface AccountApi {

    void getLogin(String userName, String password, LoginListener listener);

    void getSalaryOperation(Employee employee, int amount, Operation operation, OperationListener listener);


    interface OperationListener {

        void onOperation(boolean bool);
    }

    interface LoginListener {

        void onLogin(Employee employee);
    }
}
