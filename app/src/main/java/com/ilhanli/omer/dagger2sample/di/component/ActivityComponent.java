package com.ilhanli.omer.dagger2sample.di.component;

import com.ilhanli.omer.dagger2sample.BaseActivity;
import com.ilhanli.omer.dagger2sample.di.module.ActivityModule;
import com.ilhanli.omer.dagger2sample.di.scope.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);
}

