package com.ilhanli.omer.dagger2sample;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.ilhanli.omer.dagger2sample.business.service.AccountApi;
import com.ilhanli.omer.dagger2sample.data.DataManager;
import com.ilhanli.omer.dagger2sample.di.component.ActivityComponent;
import com.ilhanli.omer.dagger2sample.di.component.DaggerActivityComponent;
import com.ilhanli.omer.dagger2sample.di.module.ActivityModule;
import com.ilhanli.omer.dagger2sample.utility.SessionUtil;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {


    protected ProgressDialog progressDialog;

    protected SessionUtil sessionUtil;

    protected Gson gson;

    private ActivityComponent activityComponent;

    @Inject
    DataManager mDataManager;

    @Inject
    protected AccountApi accountApi;

    protected abstract int getContentViewId();

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .applicationComponent(App.get(getApplicationContext()).getActivityComponent())
                    .activityModule(new ActivityModule(this))
                    .build();
        }
        return activityComponent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(getContentViewId());

        ButterKnife.bind(this);

        getActivityComponent().inject(this);

        initBase();
    }

    public void initBase() {

        gson = new Gson();

        sessionUtil = new SessionUtil(this);

        progressDialog = new ProgressDialog(this);

        progressDialog.setCancelable(true);
    }

    public void hideProgress() {

        if (progressDialog != null) {

            if (progressDialog.isShowing()) {

                progressDialog.dismiss();
            }
        }
    }

    public void showProgress(String message) {

        if (progressDialog != null) {

            progressDialog.setMessage(message);

            if (!progressDialog.isShowing()) {

                progressDialog.show();
            }
        }
    }


}
