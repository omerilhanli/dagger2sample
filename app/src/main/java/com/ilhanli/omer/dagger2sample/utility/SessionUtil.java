package com.ilhanli.omer.dagger2sample.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.ilhanli.omer.dagger2sample.common.BundleAttribute;

public class SessionUtil {

    private static SharedPreferences sessionPreference;

    private static SharedPreferences.Editor sessionEditor;

    @SuppressLint("CommitPrefEdits")
    public SessionUtil(Context context) {

        if (sessionPreference == null) {

            sessionPreference = context.getSharedPreferences(BundleAttribute.SESSIOM_PREF, Context.MODE_PRIVATE);
        }

        if (sessionEditor == null) {

            sessionEditor = context.getSharedPreferences(BundleAttribute.SESSIOM_PREF, Context.MODE_PRIVATE)
                    .edit();
        }
    }

    public void clearSession() {

        sessionEditor.clear().apply();
    }


    public void persistSession(String key, String value) {

        sessionEditor.putString(key, value).apply();
    }

    public String checkSessionData(String key) {

        return sessionPreference.getString(key, "");
    }
}
