package com.ilhanli.omer.dagger2sample;

import android.app.Application;
import android.content.Context;

import com.ilhanli.omer.dagger2sample.di.component.ApplicationComponent;
import com.ilhanli.omer.dagger2sample.di.component.DaggerApplicationComponent;
import com.ilhanli.omer.dagger2sample.di.module.ApplicationModule;

public class App extends Application {

    private ApplicationComponent activityComponent;

    @Override
    public void onCreate() {

        super.onCreate();

        activityComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getActivityComponent() {

        return activityComponent;
    }

    public static App get(Context context) {

        return (App) context.getApplicationContext();
    }
}
