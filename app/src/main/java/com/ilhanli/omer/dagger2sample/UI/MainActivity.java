package com.ilhanli.omer.dagger2sample.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ilhanli.omer.dagger2sample.BaseActivity;
import com.ilhanli.omer.dagger2sample.R;
import com.ilhanli.omer.dagger2sample.business.service.AccountApi;
import com.ilhanli.omer.dagger2sample.common.BundleAttribute;
import com.ilhanli.omer.dagger2sample.data.model.Employee;
import com.ilhanli.omer.dagger2sample.utility.TransationUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.edit_text_username)
    EditText editTextUsername;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.button_login)
    Button buttonLogin;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        checkSession();
    }

    public void checkSession() {

        String employeeString = sessionUtil.checkSessionData(BundleAttribute.EMPLOYEE_SESSION);

        if (employeeString != null && !employeeString.isEmpty()) {

            Employee employee = gson.fromJson(employeeString, Employee.class);

            TransationUtil.startActivity(MainActivity.this,SecondActivity.class, employee);
        }
    }

    @OnClick(R.id.button_login)
    public void doLogin() {

        final String username = editTextUsername.getText().toString();

        final String password = editTextPassword.getText().toString();

        if ((username == null || password == null)
                ||
                username.isEmpty() || password.isEmpty()) {

            Log.e("ERROR-E00", "Kullanıcı adı veya parola giriş hatası.");
        }

        if (username.isEmpty()) {

            Log.e("ERROR-E010", "Kullanıcı adı girin..");
        }

        if (password.isEmpty()) {

            Log.e("ERROR-E011", "Parola girin..");
        }

        showProgress("Login loading...");

        accountApi.getLogin(username, password, new AccountApi.LoginListener() {

            @Override
            public void onLogin(final Employee employee) {

                if (employee != null) {

                    boolean status = employee.isLoginStatus();

                    if (status) {

                        String employeeString = gson.toJson(employee, Employee.class);

                        sessionUtil.persistSession(BundleAttribute.EMPLOYEE_SESSION, employeeString);

                        TransationUtil.startActivity(MainActivity.this,SecondActivity.class, employee);

                    } else {

                        Toast.makeText(MainActivity.this,
                                "Kullanıcı adınız veya parolanız hatalı, Lütfen tekrar girin.",
                                Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(MainActivity.this,
                            "Sistemde böyle bir kullanıcı bulunamadı!",
                            Toast.LENGTH_SHORT).show();
                }

                hideProgress();
            }
        });
    }
}
