package com.ilhanli.omer.dagger2sample.business;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import com.ilhanli.omer.dagger2sample.data.MockEmployees;
import com.ilhanli.omer.dagger2sample.data.model.Employee;
import com.ilhanli.omer.dagger2sample.business.service.AccountApi;
import com.ilhanli.omer.dagger2sample.utility.NetworkUtility;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AccountApiImpl implements AccountApi {

    private Context context;

    @Inject
    public AccountApiImpl(Context context) {

        this.context = context;
    }

    @Override
    public void getLogin(final String username, final String password,

                         final AccountApi.LoginListener listener) {


        if (NetworkUtility.isConnect(context)) {


            Handler hand = new Handler();

            hand.postDelayed(new Runnable() {

                @Override
                public void run() {

                    Employee employee = validation(username, password);

                    listener.onLogin(employee);

                }
            }, 1500);


        } else {


            Toast.makeText(context, "Network error!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getSalaryOperation(Employee employee, int amount, Operation operation,

                                   final AccountApi.OperationListener listener) {

        System.out.println("Welcome " + employee.getEmployeeFirstName());

        employee.setEmployeeSalary(

                operation == Operation.TAKE ?

                        employee.getEmployeeSalary() - amount :

                        employee.getEmployeeSalary() + amount);

        System.out.println("Your salary REMAIN: " + employee.getEmployeeSalary());


        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {

                listener.onOperation(true);
            }
        }, 1500);
    }


    private Employee validation(String username, String password) {

        List<Employee> employees = MockEmployees.employees();


        for (int i = 0; i < employees.size(); i++) {

            Employee employeeFromList = employees.get(i);

            String employeeUsername = employeeFromList.getEmployeeUsername();

            String employeePassword = employeeFromList.getEmployeePassword();


            if (employeeUsername.equals(username) && employeePassword.equals(password)) {

                employeeFromList.setLoginStatus(true);

                return employeeFromList;
            }
        }

        return null;
    }
}
