package com.ilhanli.omer.dagger2sample.UI;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ilhanli.omer.dagger2sample.BaseActivity;
import com.ilhanli.omer.dagger2sample.R;
import com.ilhanli.omer.dagger2sample.business.Operation;
import com.ilhanli.omer.dagger2sample.business.service.AccountApi;
import com.ilhanli.omer.dagger2sample.common.BundleAttribute;
import com.ilhanli.omer.dagger2sample.data.model.Employee;
import com.ilhanli.omer.dagger2sample.utility.TransationUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class SecondActivity extends BaseActivity {

    @BindView(R.id.text_view_id)
    TextView textViewId;
    @BindView(R.id.text_view_first_name)
    TextView textViewFirst;
    @BindView(R.id.text_view_salary)
    TextView textViewSalary;
    @BindView(R.id.button_take_money)
    Button buttonTakeMoney;
    @BindView(R.id.button_save_money)
    Button buttonSaveMoney;
    @BindView(R.id.edit_text_amount)
    EditText editTextAmount;

    private Employee employee;


    @Override
    protected int getContentViewId() {
        return R.layout.activity_second;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        initUI();
    }

    public void initUI() {

        Bundle arg = getIntent().getExtras();

        if (arg != null) {

            employee = (Employee) arg.get(BundleAttribute.EMPLOYEE);

            if (employee != null) {

                textViewId.setText(String.valueOf(employee.getEmployeeId()));

                textViewFirst.setText(employee.getEmployeeFirstName() + " " + employee.getEmployeeLastName());

                textViewSalary.setText(String.valueOf(employee.getEmployeeSalary()));
            }
        }
    }

    @OnClick(R.id.button_take_money)
    public void takeMoney() {

        String amountString = editTextAmount.getText().toString();

        if (amountString.isEmpty()) {

            Toast.makeText(this, "Lütfen bir rakam girin", Toast.LENGTH_SHORT).show();

            return;
        }

        int amount = Integer.valueOf(amountString);

        showProgress("Take loading...");

        accountApi.getSalaryOperation(employee, amount, Operation.TAKE,

                new AccountApi.OperationListener() {

                    @Override
                    public void onOperation(boolean status) {

                        if (status) {

                            textViewSalary.setText(String.valueOf(employee.getEmployeeSalary()));
                        }

                        hideProgress();
                    }
                });
    }

    @OnClick(R.id.button_save_money)
    public void saveMoney() {

        String amountString = editTextAmount.getText().toString();

        if (amountString.isEmpty()) {

            Toast.makeText(this, "Lütfen bir rakam girin", Toast.LENGTH_SHORT).show();

            return;
        }

        int amount = Integer.valueOf(amountString);

        showProgress("Save loading...");

        accountApi.getSalaryOperation(employee, amount, Operation.SAVE,

                new AccountApi.OperationListener() {

                    @Override
                    public void onOperation(boolean status) {

                        if (status) {

                            textViewSalary.setText(String.valueOf(employee.getEmployeeSalary()));
                        }

                        hideProgress();
                    }
                });
    }

    @OnClick(R.id.image_view_logout)
    public void eventLogout() {

        TransationUtil.startActivity(SecondActivity.this, MainActivity.class);

        sessionUtil.clearSession();
    }


}
